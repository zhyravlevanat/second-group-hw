mocha.setup('bdd');

const assert = chai.assert;

describe('onChange', function() {
  let sandbox;

	before(() => {
		sandbox = sinon.createSandbox();
	});

	afterEach(() => {
		sandbox.restore();
  });
  
  it('should call setColor with value "#666"', function() {
    const stub = sandbox.stub(window, 'setColor'),
          event = {
            target: {
              value: '#666',
              dataset: {
                pick: 'color'
              }
            }
          };

		onChange(event);

		sandbox.assert.calledOnce(stub);
		sandbox.assert.calledWith(stub, event.target.value);
  });

  it('should call setLineWidth with value "10"', function() {
    const stub = sandbox.stub(window, 'setLineWidth'),
          event = {
            target: {
              value: 10,
              dataset: {
                pick: 'lineWidth'
              }
            }
          };

		onChange(event);

		sandbox.assert.calledOnce(stub);
		sandbox.assert.calledWith(stub, event.target.value);
  });
});

describe('onClick', function() {
  let sandbox;
  
  before(() => {
		sandbox = sinon.createSandbox();
	});

	afterEach(() => {
		sandbox.restore();
  });

  it('figure should equal "line" if dataset.figure is undefined', function() {
    const event = {
      target: {
        dataset: {}
      }
    };

    onClick(event);
    const actual = figure;

    const expected = 'line';
    assert.equal(actual, expected);
  });

  it('figure should equal "ellipse" if dataset.figure is "ellipse"', function() {
    const event = {
      target: document.querySelector('[data-figure="ellipse"]')
    };

    onClick(event);
    const actual = figure;

    const expected = 'ellipse';
    assert.equal(actual, expected);
  });

  it('figure should equal "line" if dataset.figure is "line"', function() {
    const event = {
      target: document.querySelector('[data-figure="line"]')
    };

    onClick(event);
    const actual = figure;

    const expected = 'line';
    assert.equal(actual, expected);
  });

  it('should call toggleClass with (event.target, activeClass)', function() {
    const stub = sandbox.stub(window, 'toggleClass'),
          event = {
            target: document.querySelector('[data-figure="ellipse"]')
          },
          activeClass = 'btn--active';

    onClick(event);

		sandbox.assert.calledOnce(stub);
		sandbox.assert.calledWith(stub, event.target, activeClass);
  });
});

describe('onStartDraw', function() {
  it('isDrawing should equal true', function() {
    const event = {
            clientX: 1,
            clientY: 1,
          };

    onStartDraw(event);
    const actual = isDrawing;

    const expected = true;
    assert.equal(actual, expected);
  });

  it('startX should equal 1', function() {
    const event = {
      clientX: 1,
      clientY: 1,
    };

    onStartDraw(event);
    const actual = startX;

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('startY should equal 1', function() {
    const event = {
      clientX: 1,
      clientY: 1,
    };

    onStartDraw(event);
    const actual = startY;

    const expected = 1;
    assert.equal(actual, expected);
  });
});

describe('onDraw', function() {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call getFigureSize with (event.clientX, event.clientY)', function() {
    const stub = sandbox.stub(window, 'getFigureSize'),
          event = {
            clientX: 0,
            clientY: 0
          };
    startX = 0;
    startY = 0;
    figure = 'line';

    onDraw(event);

    sandbox.assert.calledOnce(stub);
		sandbox.assert.calledWith(stub, event.clientX, event.clientY);
  });
})

describe('onEndDraw', function() {
  it('isDrawing should equal fasle', function() {
    onEndDraw();
    const actual = isDrawing;

    const expected = false;
    assert.equal(actual, expected);
  })
});

describe('setColor', function() {
  it('color should equal "#000" if an argument is empty', function() {
    setColor();
    const actual = color;

    const expected = '#000';
    assert.equal(actual, expected);
  });

  it('color should equal "#000" if an argument is invalid', function() {
    const data = 'sdfsdf';

    setColor(data);
    const actual = color;

    const expected = '#000';
    assert.equal(actual, expected);
  });

  it('color should equal "#666666" if an argument is "#666666"', function() {
    const data = '#666666';

    setColor(data);
    const actual = color;

    const expected = '#666666';
    assert.equal(actual, expected);
  });
});

describe('setLineWidth', function() {
  it('lineWidth should equal 1 if an argument is empty', function() {
    setLineWidth();
    const actual = lineWidth;

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('lineWidth should equal 1 if an argument is invalid', function() {
    const data = 'sdfsdf';

    setLineWidth(data);
    const actual = lineWidth;

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('lineWidth should equal 1 if an argument is less than 0', function() {
    const data = -3;

    setLineWidth(data);
    const actual = lineWidth;

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('lineWidth should equal 1 if an argument is more than 15', function() {
    const data = 30;

    setLineWidth(data);
    const actual = lineWidth;

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('lineWidth should equal 15 if an argument is 15', function() {
    const data = 30;

    setLineWidth(data);
    const actual = lineWidth;

    const expected = 1;
    assert.equal(actual, expected);
  });
});

describe('getFigureSize', function() {
  it('shound return an object if the first argument is invalid', function() {
    const x = 'dfdf',
          y = 1;
    startX = 0;
    startY = 0;

    const actual = getFigureSize(x, y);

    const expected = { x: 0, y: 0, width: 0, height: 1 };
    assert.deepEqual(actual, expected);
  });

  it('shound return an object if the second argument is invalid', function() {
    const x = 1,
          y = 'sdfdsf';
    startX = 0;
    startY = 0;

    const actual = getFigureSize(x, y);

    const expected = { x: 0, y: 0, width: 1, height: 0};
    assert.deepEqual(actual, expected);
  });

  it('shound return an object', function() {
    const x = 1,
          y = 1;
    startX = 0;
    startY = 0;

    const actual = getFigureSize(x, y);

    const expected = { x: 0, y: 0, width: 1, height: 1};
    assert.deepEqual(actual, expected);
  });
});

describe('toggleClass', function() {
  it('should delete activeClass from all elements', function() {
    const btn = document.querySelector('[data-figure="line"]');

    toggleClass(btn, activeClass);
    const actual = Object.values(buttons).filter(item => item.classList.contains(activeClass)).length;

    const expected = 1;
    assert.equal(actual, expected)
  });

  it('should add class activeClass to active btn', function() {
    const btn = document.querySelector('[data-figure="line"]');

    toggleClass(btn, activeClass);
    const actual = btn.classList.contains(activeClass);

    const expected = true;
    assert.equal(actual, expected);
  });
});

mocha.run();