const canvas = document.getElementById('paint'),
      context = canvas.getContext('2d'),
      colorPicker = document.querySelector('input[type=color]'),
      rangerPicker = document.querySelector('input[type=range]'),
      buttons = document.querySelectorAll('.btn'),
      activeClass = 'btn--active';

let isDrawing = false,
    figure = 'line',
    color = '#000',
    lineWidth = 1,
    startX = 0,
    startY = 0;

// EVENT HANDLERS
window.addEventListener('change', onChange);
window.addEventListener('click', onClick);
canvas.addEventListener("mousedown" , onStartDraw);
canvas.addEventListener("mousemove", onDraw);
canvas.addEventListener("mouseup", onEndDraw);

// HANDLER FUNCTIONS
function onChange(event) {
  if (!event || !event.target) {
    return void(0);
  };

  const target = event.target;

  switch (target.dataset.pick) {
    case 'color':
      setColor(target.value);
      break;
    
    case 'lineWidth':
      setLineWidth(+target.value);
      break;
  }
}

function onClick(event) {
  if (!event || !event.target || !event.target.dataset.figure) {
    return void(0);
  }

  figure = event.target.dataset.figure;

  toggleClass(event.target, activeClass);
}

function onStartDraw(event) {
  isDrawing = true;
  startX = event.clientX;
  startY = event.clientY;
  context.strokeStyle = color;
  context.lineWidth = lineWidth;

  if (figure === 'line') {
    context.beginPath();
  };

  context.moveTo(startX, startY);
}

function onDraw(event) {
  const figureSize = getFigureSize(event.clientX, event.clientY);

  switch (isDrawing && figure) {
    case 'square':
      drawSquare(figureSize);
      break;

    case 'ellipse':
      drawEllipse(figureSize);
      break;
    
    case 'curve':
      drawCurve(figureSize);
      break;

    case 'line':
      drawLine(event.clientX, event.clientY);
      break;
  }
}

function onEndDraw() {
  if (figure !== 'line') {
    context.stroke();
  };

  isDrawing = false;
}

// SETTER FUNCTIONS
function setColor(str) {
  if (!str || str.indexOf('#') < 0) {
    return;
  }

  color = str;
}

function setLineWidth(num) {
  if (!+num || num < 0 || num > 15) {
    return;
  }

  lineWidth = num;
}

// DRAWING FUNCTIONS
function drawLine(x, y) {
  context.lineTo(x, y);
  context.stroke();
}

function drawSquare(figureSize) {
  context.beginPath();
  context.lineJoin = "miter";
  context.rect(figureSize.x, figureSize.y, figureSize.width, figureSize.height);
}

function drawEllipse(figureSize) {
  console.log(figureSize);
  const radiusX = figureSize.width / 2,
        radiusY = figureSize.height / 2,
        x = figureSize.x + radiusX,
        y = figureSize.y + radiusY;

  context.beginPath();
  context.ellipse(x, y, radiusX, radiusY, 0, 0, 2 * Math.PI);
}

function drawCurve(figureSize) {
  context.beginPath();
  context.lineJoin = "round";
  context.rect(figureSize.x, figureSize.y, figureSize.width, figureSize.height);
}

// HELPERS

function getFigureSize(x, y) {
  x = (typeof x === 'number') ? x : 0;
  y = (typeof y === 'number') ? y : 0;

  return {
    x: Math.min(x, startX),
    y: Math.min(y, startY),
    width: Math.abs(x - startX),
    height: Math.abs(y - startY)
  };
}

function toggleClass(elem, cssClass) {
  buttons.forEach((item) => {
    item.classList.remove(cssClass);
  });

  elem.classList.add(cssClass);
}