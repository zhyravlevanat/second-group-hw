mocha.setup('bdd');

const assert = chai.assert;

describe('draw', function() {
  let sandbox = null;

	before(() => {
		sandbox = sinon.createSandbox();
	});

	afterEach(() => {
		sandbox.restore();
  });

  it('should call beginPath in draw', function() {
    const ball = new Ball();    
    const stub = sandbox.stub(ctx, 'beginPath');

    ball.draw();

    sandbox.assert.calledOnce(stub);
  });

  it('should call arc in draw', function() {
    const ball = new Ball();    
    const stub = sandbox.stub(ctx, 'arc');

    ball.draw();

    sandbox.assert.calledOnce(stub);
  });

  it('should call fill in draw', function() {
    const ball = new Ball();    
    const stub = sandbox.stub(ctx, 'fill');

    ball.draw();

    sandbox.assert.calledOnce(stub);
  });

  it('should call stroke in draw', function() {
    const ball = new Ball();    
    const stub = sandbox.stub(ctx, 'stroke');

    ball.draw();

    sandbox.assert.calledOnce(stub);
  });  
});

describe('updateCoords', function() {
  it('should update coords on velocity values', function() {
    const ball = new Ball();
    ball.x = 560;
    ball.y = 770;
    ball.velocityX = 5;
    ball.velocityY = -4;

    updateCoords(ball);
    const actual = {
      x: ball.x,
      y: ball.y
    }

    const expected = {
      x: 565,
      y: 774
    };
    assert.deepEqual(actual, expected);
  });
});

describe('absorbBall', function() {
  it('should delete ball with lesser radius and bigger ball make bigger on radius of less ball', function() {
    const ball1 = {
      x: 200,
      y: 500,
      radius: 20
    };
    const ball2 = {
      x: 215,
      y: 500,
      radius: 15
    };

    absorbBall(ball1, ball2);
    const actual = ball1.radius;

    const expected = 35;
    assert.deepEqual(actual, expected);
  });
});

describe('addBall', function() {
  it('should create new ball and add it to array of balls', function() {
    balls.length = 0;
    const ball1 = {
      x: 200,
      y: 500,
      radius: 15
    };
        
    addBall(ball1.x, ball1.y);
    const actual = balls.length;
    
    const expected = 1;
    assert.deepEqual(actual, expected);
  })
});

describe('deleteBall', function() {
  it('should delete ball from array of balls', function() {
    balls.length = 0;
    const ball = {
      x: 200,
      y: 500,
      velocityX: 2, 
      velocityY: -2,
      color: 'rgb(0, 0, 0)'
    };
    balls[0] = ball;

    deleteBall(ball);
    const actual = balls;

    const expected = [];
    assert.deepEqual(actual, expected);
  })
});


describe('isCollide', function() {
  it('should return true if balls collided', function() {
    const ball1 = {
      x: 200,
      y: 500,
      radius: 20
    };
    const ball2 = {
      x: 215,
      y: 500,
      radius: 15
    };

    const actual = isCollide(ball1, ball2);

    const expected = true;
    assert.deepEqual(actual, expected);
  })
});

// describe('splitBall', function() {
//   it('should return true if balls collided', function() {
//     balls.length = 0;
//     const ball = {
//       x: 200,
//       y: 500,
//       radius: 20
//     };
//     event = {
//       clientX: 200,
//       clientY: 300,
//     };
    

//     splitBall(ball);
//     const actual = balls.length;

//     const expected = 8;
//     assert.deepEqual(actual, expected);
//     balls.length = 0;
//   })
// });

// describe('render', function() {
//   let sandbox;
  
//     before(() => {
//       sandbox = sinon.createSandbox();
//     });
  
//     afterEach(() => {
//       sandbox.restore();
//     });

//     it('should call updateCoords  function', function() {
      
//       const stub = sandbox.stub(window, 'updateCoords');
      
// 		  render();
  
//       sandbox.assert.calledOnce(stub);      
//     });  
// });

describe('init', function() {
  let sandbox;
  
    before(() => {
      sandbox = sinon.createSandbox();
    });
  
    afterEach(() => {
      sandbox.restore();
    });

    it('should call fillRect function', function() {
      const stub = sandbox.stub(ctx, 'fillRect');
      
		  init();
  
      sandbox.assert.calledOnce(stub);      
    });  
});

describe('getHitBall', function() {
  it('should return targetBall if it was clicked', function() {
    const event = {
      clientX: 200,
      clientY: 300,
    };
    
    const actual = getHitBall(event);

    const expected = null;
    assert.equal(actual, expected);
  });
});

describe('onClick', function() {
  let sandbox;
  
    before(() => {
      sandbox = sinon.createSandbox();
    });
  
    afterEach(() => {
      sandbox.restore();
    });

    it('should call getHitBall function', function() {
      const stub = sandbox.stub(window, 'getHitBall'),
            event = {
              clientX: 200,
              clientY: 300,
            };

      onClick(event);

      sandbox.assert.calledOnce(stub);
      sandbox.assert.calledWith(stub, event);   
    });  
});

mocha.run();